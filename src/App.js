import { useState } from 'react';
import './App.css';

function Todo(props){
  return (
    <p
      className="todo"
      id={props.id}
      onClick={props.hStrike}
      onContextMenu={props.hSppr}
    >
      {props.msg}
    </p>
  );
}

function App() {

  let [todos, setTodos] = useState([]);
  let [todoId, setTodoId] = useState(todos.length);

  let addTodo = (event) =>{
    let newTodos = [];
    if(event.code === "Enter"){
      newTodos = [...todos, {id:todoId, msg:event.target.value, done:false}];
      //initializing back the input
      event.target.value = null;
    }else newTodos = todos;
    //passing the values to the state
    setTodos(newTodos);
    setTodoId(todoId+1);
  }

  let handleTodoDelete = (event) =>{
    event.preventDefault();
    setTodos(todos.filter(todo => todo.id !== Number(event.target.id)));
  }

  let handleTodoStrike = (event) => {
    let newTodos = todos.map(todo => {
      if(todo.id === Number(event.target.id)){
        if(todo.done === false){
          event.target.style = "text-decoration:line-through";
        }else event.target.style = "text-decoration: none";

        return {...todo, done:!todo.done};
      }else return todo;
    })

    setTodos(newTodos);
  }


  return (
    <div className="App">
      <header className="App-header">
        <h1 className="App-name">todos</h1>
        <p><input type="text" className="todo" placeholder="Enter your todo" onKeyPress={addTodo}/></p>
        {
          todos.map(todo =><Todo key={todo.id} id={todo.id} msg={todo.msg} strike={todo.done} hStrike={handleTodoStrike} hSppr={handleTodoDelete} />)
        }
        <p 
          className="how">
            Left click to toggle-completed. Right click to delete todo
        </p>
      </header>
    </div>
  );
}

export default App;
